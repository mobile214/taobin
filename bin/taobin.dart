import 'dart:io';

class Taobin {
  String category;
  int price=0;
  List<String> repeatOrder = [""];

  Taobin(this.category);

  void chooseCategory(String category) {
    switch(category) {
      case "1" :
      print("1. เครื่องดื่มขายดี");
      print("1.ชาไทยนมเย็น 40฿ \n 2.นมบราวน์ชูการ์เย็น 40฿");
      break;
      case "2" :
      print("2. กาแฟ");
      print("1.ลาเต้ร้อน 35฿ \n 2.ลาเต้เย็น 40฿");
      break;
      case "3" :
      print("3. ชา");
      print("1.เก๊กฮวยร้อน 20฿ \n 2.ชาเขียวมะนาวร้อน 20฿");
      break;
      case "4" :
      print("4. นม,โกโก้ และคาราเมล");
      print("1.นมร้อน 30฿ \n 2.โกโก้เย็น 35฿");
      break;
      case "5" :
      print("5. โปรตีนเชค");
      print("1.มัทฉะโปรตีน 35฿ \n 2.โกโก้โปรตีน 35฿");
      break;
      case "6" :
      print("6. โซดา และอื่นๆ");
      print("1.น้ำลิ้นจี่โซดา 25฿ \n 2.โซดา 10฿");
      break;

    }

  }

    void chooseMenu() {
      print("----------------------");
      print("กรุณาเลือกเมนู");
        switch(category) {
          case "1" : //เครื่องดื่มขายดี
          String menu = stdin.readLineSync()!;
          switch (menu) {
            case "1" :
            print("1.ชาไทยนมเย็น 40฿");
            price = 40;
            repeatOrder.add("ชาไทยนมเย็น 40฿");
            break;
            case "2" :
            print("2.นมบราวน์ชูการ์เย็น 40฿");
            price = 40;
            repeatOrder.add("นมบราวน์ชูการ์เย็น 40฿");
            break;
          }
          break;
          case "2" : //กาแฟ
          String menu = stdin.readLineSync()!;
          switch (menu) {
            case "1" :
            print("1.ลาเต้ร้อน 35฿");
            price = 35;
            repeatOrder.add("ลาเต้ร้อน 35฿");
            break;
            case "2" : 
            print("2.ลาเต้เย็น 40฿");
            price = 40;
            repeatOrder.add("ลาเต้เย็น 40฿");
            break;
          }
          break;
          case "3" : //ชา
          String menu = stdin.readLineSync()!;
          switch (menu) {
            case "1" :
            print("1.เก๊กฮวยร้อน 20฿");
            price = 20;
            repeatOrder.add("เก๊กฮวยร้อน 20฿");
            break;
            case "2" :
            print("2.ชาเขียวมะนาวร้อน 20฿");
            price = 20;
            repeatOrder.add("ชาเขียวมะนาวร้อน 20฿");
            break;
          }
          break;
          case "4" : //นม,โกโก้
          String menu = stdin.readLineSync()!;
          switch (menu) {
            case "1" :
            print("1.นมร้อน 30฿");
            price = 30;
            repeatOrder.add("นมร้อน 30฿");
            break;
            case "2" :
            print("2.โกโก้เย็น 40฿");
            price = 40;
            repeatOrder.add("โกโก้เย็น 40฿");
            break;
          }
          break;
          case "5" : //โปรตีนเชค
          String menu = stdin.readLineSync()!;
          switch (menu) {
            case "1" :
            print("1.มัทฉะโปรตีน 35฿");
            price = 35;
            repeatOrder.add("มัทฉะโปรตีน 35฿");
            break;
            case "2" :
            print("2.โกโก้โปรตีน 35฿");
            price = 35;
            repeatOrder.add("โกโก้โปรตีน 35฿");
            break;
          }
          break;
          case "6" : //โซดาและอื่นๆ
          String menu = stdin.readLineSync()!;
          switch (menu) {
            case "1" :
            print("1.น้ำลิ้นจี่โซดา 25฿");
            price = 25;
            repeatOrder.add("น้ำลิ้นจี่โซดา 25฿");
            break;
            case "2" :
            print("2.โซดา 10฿");
            price = 10;
            repeatOrder.add("โซดา 10฿");
            break;
          }
          break;
        }
        print("----------------------");
    }

    void chooseLevelOfSugar() {
      print("กรุณาระดับความหวาน");
      print("1. น้อย");
      print("2. ปกติ");
      print("3. มาก");
      print("----------------------");

      String levelOfsugar = stdin.readLineSync()!;

      switch(levelOfsugar) {
        case "1" :
        print("1. น้อย");
        repeatOrder.add("หวานน้อย");
        break;
        case "2" :
        print("2. ปกติ");
        repeatOrder.add("หวานปกติ");
        break;
        case "3" :
        print("3. มาก");
        repeatOrder.add("หวานมาก");
        break;
      }
    }

    void payMoney() {
      print(repeatOrder);
      print("กรุณาใส่เงินตามจำนวน");
      int money = int.parse(stdin.readLineSync()!);
      if(money<price) {
        print("กรุณาใส่เงินเพิ่ม");
        money = int.parse(stdin.readLineSync()!);
      }else{
        int change = money-price;
        print("กรุณารับเงินทอน");
        print(change);
      }
      print("กรุณารอรับเครื่องดื่ม");
      print("----------------------");
      print("Thank You!!");
      print("----------------------");
    }

  }

